

let fs = require('fs');
function ReadFile(textname)
{
    return new Promise(function(resolve, reject)
    {
        fs.readFile(textname, 'utf8', function(err, data)
        {
            if (err)
            reject(err);
            else resolve(data);
        });
    });
}
function writeFile(textname,text)
{
    return new Promise(function(resolve, reject)
    {
        fs.writeFile(textname, text, 'utf8', function(err)
        {
            if (err)
            reject(err);
            else
            resolve(true);
        });
    });
}
function GetEyeColors()
{
    return new Promise(async function(resolve, reject)
    {
      let jsondata= await ReadFile('homework1-4.json');
      let eyecolor={};
      let broweye =0;
      let blueeye =0;
      let greeneye =0;
      let Datajson={};
       Datajson=JSON.parse(jsondata);
      for(i=0;i<Datajson.length;i++)
      {
          for(j in Datajson[i])
          {
              if(j=="eyeColor")
              {
                  if(Datajson[i][j]=="brown")
                  {
                    broweye++;
                  }
              
                  if(Datajson[i][j]=="blue")
                  {
                    blueeye++;
                  }
              
                  if(Datajson[i][j]=="green")
                  {
                    greeneye++;
                  }
              }
          }           
      }
      eyecolor.brown=broweye;
      eyecolor.blue=blueeye;
      eyecolor.green=greeneye;
        resolve(eyecolor);
       
    });
}

function GetGender()
{
    return new Promise(async function(resolve, reject)
    {
        let jsondata=await ReadFile('homework1-4.json');
        let gender={};
       let eye={};
       let male=0;
       female=0;
        
        jsonobj=JSON.parse(jsondata);
    for(i=0;i<jsonobj.length;i++)
    {
     for(let j in jsonobj[i])
     {
         if(j=='gender')
         {
          if(jsonobj[i][j]=="male")
          {
            male++;
          }
          if(jsonobj[i][j]=="female")
          {
            female++;
          }
         
         }
     }
    }
    
    gender.male=male;
    gender.female=female;  
        resolve(gender);
       
    });
}
function GetFrinds()
{
    return new Promise(async function(resolve, reject)
    {
        let jsondata=await ReadFile('homework1-4.json');
        let jsonobj=[];
    let frinds={};
  let arrfrinds=[];
   
    jsonobj=JSON.parse(jsondata);
for(i=0;i<jsonobj.length;i++)
{
 for(let j in jsonobj[i])
 {
    if(j=='_id')
    {
      frinds.id= jsonobj[i][j];
    }
     if(j=='friends')
     {
        frinds.frindscount= jsonobj[i][j].length;
     }
    
 }
 arrfrinds.push(frinds);
 frinds={};
 
}  
resolve(arrfrinds); 
       
    });
}
function countFrinds()
{
    return new Promise(async function(resolve, reject)
    {
        let frindsdata =await GetFrinds();
        let countfrinds=Object.keys(frindsdata).reduce(function (previous,key) {
            return previous + frindsdata[key];
        }, 0);
       
        resolve(countfrinds); 
       
    });
}
function countEyecount()
{
    return new Promise(async function(resolve, reject)
    {
        let eye=await GetEyeColors();
    let counteyecolor =Object.keys(eye).reduce(function (previous, key) {
        return previous + eye[key];
    }, 0);
    resolve(counteyecolor);
});
}
function countFrinds()
{
    return new Promise(async function(resolve, reject)
    {
        let frinds=await GetFrinds();
   let frindscount=frinds.length;
    resolve(frindscount);
});
}
function countGender()
{
    return new Promise(async function(resolve, reject)
    {
        let Gender=await GetGender();
    let countGender =Object.keys(Gender).reduce(function (previous, key) {
        return previous + Gender[key];
    }, 0);
    resolve(countGender);
});
}
async function WriteFileEyecolor()
{ try
    { 
         
       let eyecolordata =await GetEyeColors();
       let eyecolorobj=JSON.stringify(eyecolordata);
       await writeFile("homework5-1_eyes.json",eyecolorobj); 
    }
    catch (error)
    {
        console.error(error);
    }
}
async function WriteFileGender()
{ try
    { 
        let genderdata =await GetGender();
        let genderobj =JSON.stringify(genderdata);
        await writeFile("homework5-1_gender.json",genderobj);  
    }
    catch (error)
    {
        console.error(error);
    }
}
async function WriteFileFriends()
{ try
    { 
        let frindsdata =await GetFrinds();
        let frindsobj =JSON.stringify(frindsdata);
        await writeFile("homework5-1_friends.json",frindsobj);  
    }
    catch (error)
    {
        console.error(error);
    }
}

WriteFileEyecolor();
WriteFileGender();
WriteFileFriends();

exports.ReadFile=ReadFile;
exports.GetEyeColors=GetEyeColors;
exports.GetGender=GetGender;
exports.GetFrinds=GetFrinds;
exports.countEyecount=countEyecount;
exports.frindscount=countFrinds;
exports.countGender=countGender;