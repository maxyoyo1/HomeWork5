let assert = require('assert');
let homework =require('./homework5-1.js');
let fs = require('fs');
let readfile=homework.ReadFile;
let eyecolorobj=homework.GetEyeColors;
let genderobj=homework.GetGender;
let getfrinds=homework.GetFrinds;
let counteye = homework.countEyecount;
let frindscount = homework.frindscount;
let Gendercount =homework.countGender;


  describe('tdd lab', function() {
    describe('#checkFileExist()', function() {
          it('should have homework5-1_eyes.json existed',async() => {
         let eye= await readfile("homework5-1_eyes.json");
         let eyedata =JSON.parse(eye);
         assert.strictEqual(typeof eyedata  === "object", true, "homework5-1_eyes.json should be true format");
          });
          it('should have homework5-1_gender.json existed', async() => {
            let gender = await readfile("homework5-1_gender.json");
            let genderdata =JSON.parse(gender);
            assert.strictEqual(typeof genderdata  === "object", true, "homework5-1_gender.json should be true format");
          });
          it('should have homework5-1_friends.json existed', async() => {
            let friends= await readfile("homework5-1_friends.json");
            let friendsdata =JSON.parse(friends);
            assert.strictEqual(typeof friendsdata  === "object", true, "homework5-1_friends.json should be true format");
          });
      });
      describe('#objectKey()', function() {
  
          it('should have same object key stucture as homework5-1_eyes.json', async() => {
              let Eyecolorobj = await eyecolorobj();    
               
            let myVariable = {brown:11,green:6,blue:6};
            assert.deepEqual(myVariable, Eyecolorobj,'Obj myVariable'); 
          });
          it('should have same object key stucture as homework5-1_friends.json', async() => {
            let Frindsobj = await getfrinds(); 
            let jsondata = await readfile("homework1-4.json"); 
            let myVariable = objfrinds(jsondata)
            assert.deepEqual(myVariable,Frindsobj,'Obj myVariable');
          });
          it('should have same object key stucture as homework5-1_gender.json', async() => {
            let Genderobj = await genderobj();  
          
           let myVariable={"male":11,"female":12};
         
            assert.deepEqual(myVariable, Genderobj,'Obj myVariable');
          });
      });
      describe('#userFriendCount()', function() {
          it('should have size of array input as 23', async() => {
            let frinds =await frindscount();
            assert.deepEqual(parseInt(frinds),23);
          });
      });
      describe('#sumOfEyes()', function() {
          it('should have sum of eyes as 23', async() => {
              let eyecolors =await counteye();
              console.log(eyecolors);
              assert.deepEqual(parseInt(eyecolors),23);
          });
      });
  
      describe('#sumOfGender()', function() {
          it('should have sum of gender as 23', async() => {
            let Gender =await Gendercount();
            assert.deepEqual(parseInt(Gender),23);
          });
      });
  });



function objfrinds(jsondata)
{
    let jsonobj=[];
    let frinds={};
  let arrfrinds=[];
   
    jsonobj=JSON.parse(jsondata);
for(i=0;i<jsonobj.length;i++)
{
 for(let j in jsonobj[i])
 {
    if(j=='_id')
    {
      frinds.id= jsonobj[i][j];
    }
     if(j=='friends')
     {
        frinds.frindscount= jsonobj[i][j].length;
     }
    
 }
 arrfrinds.push(frinds);
 frinds={};
 
}  
return arrfrinds;   
}